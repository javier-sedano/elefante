Sample notes application in Caspio.

Deployed to production at https://jsedano.caspio.com/dp/7011a000a7ac34feb1ed4415be49

Keywords: no-code, low-code, caspio

Instructions:
1. Download the latest .zip export from this repo.
2. Login to caspio and import the .zip as a new application.

Useful links:
* https://www.caspio.com/
